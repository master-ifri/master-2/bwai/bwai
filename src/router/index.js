import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'auth',
    component: () => import('../views/bwai.vue')  
  },
  {
    path: '/auth',
    name: 'auth',
    component: () => import('../views/Auth.vue')  
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/avatars',
    name: 'users-avatars',
    component: () => import('../views/users/avatars.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/users',
    name: 'users-list',
    component: () => import('../views/users/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/users-profile',
    name: 'users-profile',
    component: () => import('../views/users/profile.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/articles',
    name: 'articles-list',
    component: () => import('../views/articles/list.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/reviews',
    name: 'reviews-list',
    component: () => import('../views/reviews/list.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/reviews-list',
    name: 'r-reviews-list',
    component: () => import('../views/reviews/r-list.vue'),
    meta: {
      requiresAuth: true
    }
  },

  


  {
    path: '/email-verify/:token',
    name: 'account-verification',
    component: () => import('../views/email-verify.vue'),
    meta: {
      // requiresAuth: true
    },
    props: true
  },
  {
    path: '/reset-password/:token',
    name: 'reset-password',
    component: () => import('../views/reset-password.vue'),
    props: true

  }
  // {
  //   path: '/resetpassword/:id',
  //   name: 'resetpassword',
  //   component: () => import('../views/reset-password'),
  //   meta: {
  //     // requiresAuth: true
  //   },
  //   props: true
  // },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!localStorage.getItem('token')) {
      next({path: '/'});
      console.log("Token ", localStorage.getItem('token'));
    } else {
      // if (to.path == "/home") {
      //   // console.log("==========> Halloooooooo");
      //   next({ path: '/home' });
      // } else {
        // next({ path: '/' });
        next();
      // }
    }
  } else {
    next();
  }
})

export default router
